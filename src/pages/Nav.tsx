import React, { FC, useState, useContext, useEffect } from 'react';
import { Link, useLocation } from 'react-router-dom';
import { ThemeContext } from '../ThemeContext';
import SvgComponent from './SvgComponent';
import { motion } from 'framer-motion';

interface NavProps {
  setDarkMode?: any;
}

const Nav: FC<NavProps> = () => {
  const { props, setProps } = useContext(ThemeContext);
  const handleClick = () => {
    let propsCopy = JSON.parse(JSON.stringify(props));
    propsCopy.isDark = !propsCopy.isDark;
    setProps(propsCopy);
  };
  const duration = 0.3;

  const variants = {
    open: {
      opacity: 1,
      scale: 1,
      y: '0%',
      x: '0%',
      transition: { duration: duration }
    },
    closed: {
      opacity: 0,
      scale: 0,
      y: '-20%',
      x: '0%',
      transition: { duration: duration }
    }
  };

  const [menuIsOpen, setMenu] = useState(false);
  const location = useLocation();
  useEffect(() => {
    setMenu(false);
  }, [location]);

  const toggleMenu = () => {
    setMenu(!menuIsOpen);
  };

  return (
    <>
      <div>
        <div
          className={
            'py-4 md:m-auto hidden md:block ' +
            (props.isDark
              ? props.dark.bg + ' ' + props.dark.text
              : props.light.bg + ' ' + props.light.text)
          }
        >
          <div className="inline pr-8">
            <Link className="inline" to="/">
              <img
                width="32px"
                src={
                  props.isDark
                    ? 'https://minioapi.wyatthorton.xyz/portfolio/download/wh-logo.png'
                    : 'https://minioapi.wyatthorton.xyz/portfolio/download/wh-logo2.png'
                }
                className="inline"
              ></img>
              <strong>Wyatt Horton</strong>
            </Link>
          </div>
          <Link to="/works" className="px-4">
            Portfolio
          </Link>
          <Link to="/posts" className="px-4">
            Posts
          </Link>
          <button
            className={
              'px-4 border border-transparent rounded-md hover:rounded-lg ml-8 active:-translate-y-2 shadow-sm hover:shadow-black ' +
              (!props.isDark
                ? 'bg-black text-white'
                : 'bg-orange-100 text-slate-800')
            }
            onClick={handleClick}
          >
            {props.isDark ? 'Light' : 'Dark'}
          </button>
        </div>
        <div
          id="mobileMenu"
          className="md:hidden flex flex-rows justify-between"
        >
          <div className="text-left pl-4 pt-4">
            <Link to="/" className="hover:font-semibold">
              <img
                width="32px"
                src={
                  props.isDark
                    ? 'https://minioapi.wyatthorton.xyz/portfolio/download/wh-logo.png'
                    : 'https://minioapi.wyatthorton.xyz/portfolio/download/wh-logo2.png'
                }
                className="inline"
              ></img>
              Wyatt Horton
            </Link>
          </div>
          <div className="pr-4 pt-4 flex flex-rows">
            <button
              className={
                'px-1 mx-2 border border-transparent rounded-md hover:rounded-lg ml-8 active:-translate-y-2 shadow-sm hover:shadow-black ' +
                (!props.isDark
                  ? 'bg-black text-white'
                  : 'bg-orange-100 text-slate-800')
              }
              onClick={handleClick}
            >
              {props.isDark ? 'Light' : 'Dark'}
            </button>
            <button onClick={toggleMenu} className="">
              <div className="border-none shadow-sm rounded-md hover:shadow-black">
                <SvgComponent
                  width="32px"
                  fill={props.dark.svgFill}
                  stroke={
                    props.isDark ? props.dark.svgFill : props.light.svgFill
                  }
                />
              </div>
            </button>
            <motion.nav
              animate={menuIsOpen ? 'open' : 'closed'}
              variants={variants}
              className={'absolute top-16 right-4'}
            >
              <div
                className={
                  'w-32 text-left text-md border-none rounded-md ' +
                  (props.isDark ? props.dark.navBg : props.light.navBg)
                }
              >
                <ul className="pl-1">
                  <li className="hover:underline">
                    <Link to="/works">
                      <h1 className="py-2">Portfolio</h1>
                    </Link>
                  </li>
                  <li className="hover:underline">
                    <Link to="/posts">
                      <h1 className="py-2">Posts</h1>
                    </Link>
                  </li>
                </ul>
              </div>
            </motion.nav>
          </div>
        </div>
      </div>
    </>
  );
};

export default Nav;
