import ReactDOM from 'react-dom/client';
import './index.css';
import './tailwind.output.css';
import App from './App';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import About from './routes/about';
import EmptyPage from './routes/emptypage';
import Body from './pages/Body';
import Footer from './pages/Footer';
import WorkPage from './pages/WorkPage';
import { ContextProvider } from './ThemeContext';

const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);

root.render(
  <BrowserRouter>
    <Routes>
      <Route
        path="/"
        element={
          <ContextProvider>
            <App page="" />
            <Footer text="© 2022 Wyatt Horton. All Rights Reserved." />
          </ContextProvider>
        }
      >
        <Route path="" element={<Body />} />

        <Route path="works" element={<About title="about me" />} />
        <Route path="works/*" element={<WorkPage />} />
        <Route path="*" element={<EmptyPage content="invalid url" />} />
        <Route
          path="posts"
          element={<EmptyPage content="Work in Progress" />}
        />
      </Route>
    </Routes>
  </BrowserRouter>
);
