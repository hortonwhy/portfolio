import React, { FC, useState, useEffect, useContext } from 'react';
import { ThemeContext } from '../ThemeContext';

interface FooterProps {
  text: string;
}

const Footer: FC<FooterProps> = ({ text }) => {
  const { props, setProps } = useContext(ThemeContext);
  return (
    <div
      className={
        'relative bottom-0 left-0 right-0 m-auto text-center py-2 ' +
        (props.isDark
          ? props.dark.bg + ' ' + props.dark.text
          : props.light.bg + ' ' + props.light.text)
      }
    >
      {text}
    </div>
  );
};

export default Footer;
