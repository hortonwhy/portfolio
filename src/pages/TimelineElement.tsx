import { FC } from 'react';

interface TimelineElementProps {
  events: {
    year: number;
    event: string;
  }[];
}

const ProfileBubble: FC<TimelineElementProps> = ({ events }) => {
  return (
    <>
      <div className="text-justify w-fit pt-2">
        {events.map((event) => {
          return (
            <div className="grid grid-cols-7 gap-y-1 gap-x-1">
              <div className="col-span-1">
                <strong>{event.year}</strong>
              </div>
              <div className="col-span-6">{event.event}</div>
            </div>
          );
        })}
      </div>
    </>
  );
};

export default ProfileBubble;
