import { FC, useContext } from 'react';
import { Link, useLocation } from 'react-router-dom';
import { ThemeContext } from '../ThemeContext';
import { motion } from 'framer-motion';

interface WorkPageProps {}

const Works: FC<WorkPageProps> = ({}) => {
  const location = useLocation();
  const { props, setProps } = useContext(ThemeContext);

  const urlLength = location.pathname.split('/').length;
  const pageName = location.pathname.split('/')[urlLength - 1];
  const data = [
    {
      title: 'Portfolio',
      year: '2022',
      description:
        'Website using React, TailwindCSS, Framer-motion that I decided to make in order to showcase some of my other projects and make convenient medium in which people can view my work rather than read about it. This is also the website you are currently viewing',
      url: 'https://wyatthorton.xyz',
      stack: 'Node.js, React, Typescript',
      media: ['']
    },
    {
      title: 'Pet-updates',
      year: '2022',
      description:
        'A simple fullstack web panel I made for my sister and brother in law to easily keep track of their pets while away on vacation. It is deployed on my server within docker containers for both the front and backend. The front end is made with very simple styling, but makes requests to the backend api.',
      url: 'https://pets.wyatthorton.xyz',
      stack: 'Node.js, Typescript, Express, Postgres, React',
      media: [
        'https://minioapi.wyatthorton.xyz/portfolio/download/pet-updates/home.png',
        'https://minioapi.wyatthorton.xyz/portfolio/download/pet-updates/body.png',
        'https://minioapi.wyatthorton.xyz/portfolio/download/pet-updates/sql.png',
        'https://minioapi.wyatthorton.xyz/portfolio/download/pet-updates/postgres.png',
        'https://minioapi.wyatthorton.xyz/portfolio/download/pet-updates/docker.png'
      ]
    },
    {
      title: 'Linux-server',
      year: '2020-',
      description:
        '1U server rack I bought and installed Gentoo linux on as a baseline for my developing my own cloud ecosystem and my own cloud services. Hosts seafile instance, jellyfin media streaming instance, my websites, MinIO cloud bucket service, wireguard vpn, and Database instances that I require such as Postgres. I believe in owning and having full control over my data. I do not believe in storing my own personal data on 3rd party cloud service providers.',
      url: '',
      stack: 'Linux, Gentoo, Docker',
      media: [
        'https://minioapi.wyatthorton.xyz/portfolio/download/server/neofetch.png',
        'https://minioapi.wyatthorton.xyz/portfolio/download/server/server-docker.png',
        'https://minioapi.wyatthorton.xyz/portfolio/download/server/lsblk.png'
      ]
    },
    {
      title: 'Phaser-game',
      year: '2021',
      description:
        'A semester long project for a game development class that I took. I was in a small group and did a lot of the core mechanics such as player movement, player inventory, enemy spawn logic, and world generation. It was a good long term project that made solving little problems satisfying. The class focus was on working with unfamiliar people over a long period of time where reliability, communication, and diligence mattered.',
      url: 'https://slime.wyatthorton.xyz',
      stack: 'Phaser, JavaScript',
      media: [
        'https://minioapi.wyatthorton.xyz/portfolio/download/slime/mainmenu.png',
        'https://minioapi.wyatthorton.xyz/portfolio/download/slime/start.png',
        'https://minioapi.wyatthorton.xyz/portfolio/download/slime/fight.png'
      ]
    },
    {
      title: 'dokidoki-travel',
      year: '2021',
      description:
        'A collaborative project that took the form of an interactive map of Japan that allowed users to click on individual prefectures to get more information about what kind of anime and anime characters were present in each respective location. Involed a database to fetch informtion from, form validation for new users and user postings. ***note: many features are broken as I do not have access to all the dependencies and features from when it was developed!***',
      url: 'https://dokidoki.wyatthorton.xyz/index.php',
      stack: 'HTML, CSS, JavaScript, PHP, MySQL',
      media: [
        'https://minioapi.wyatthorton.xyz/portfolio/download/dokidoki/home.png',
        'https://minioapi.wyatthorton.xyz/portfolio/download/dokidoki/page.png',
        'https://minioapi.wyatthorton.xyz/portfolio/download/dokidoki/contact.png',
        'https://minioapi.wyatthorton.xyz/portfolio/download/dokidoki/about.png'
      ]
    },
    {
      title: 'ecommerce',
      year: 'May 2022- June 2022',
      description:
        'An ecommerce website I had intended to build for a friend to help with their small business, It become obvious the scope of the project was too large and that I was mostly reinventing the wheel, so I have opted to drop the project and focus on other projects more within an achievable scope. most of the interesting functionality is in the checkout page that utilizes the ecommercejs backend ***Status: Dropped, unfinished***',
      url: 'https://ecommerce.wyatthorton.xyz',
      stack: 'React, TailwindCSS, ecommercejs',
      media: [
        'https://minioapi.wyatthorton.xyz/portfolio/download/ecommerce/feature.png',
        'https://minioapi.wyatthorton.xyz/portfolio/download/ecommerce/product.png',
        'https://minioapi.wyatthorton.xyz/portfolio/download/ecommerce/checkout.png'
      ]
    }
  ];

  const entry = data.filter((v) => v.title.toLowerCase() === pageName)[0];

  const urlIsAvailable = entry.url === '' ? false : true;

  return (
    <motion.div
      animate={{ scale: [0.75, 1], opacity: [0, 1], y: [100, 0] }}
      transition={{ duration: 0.6 }}
      className="grid grid-cols-1 mt-8"
    >
      <div className="text-left">
        <Link to="/works" className="pl-16 hover:underline">
          <strong>Works</strong>
        </Link>
        {'<- '}
        <span className="">
          {entry.title}{' '}
          <span
            className={
              'p-0.5 rounded-lg ' +
              (props.isDark
                ? props.light.bg + ' ' + props.light.text
                : props.dark.bg + ' ' + props.dark.text)
            }
          >
            {entry.year}
          </span>
        </span>
        <div className="w-5/6 m-auto text-justify pt-4">
          {entry.description}
          <br></br>
          <br></br>{' '}
          <strong
            className={
              '' +
              (props.isDark
                ? props.dark.altBg + ' ' + props.dark.altText
                : props.light.altBg + ' ' + props.light.altText)
            }
          >
            Stack:
          </strong>{' '}
          {entry.stack}
          <div>
            {urlIsAvailable ? (
              <strong
                className={
                  '' +
                  (props.isDark
                    ? props.dark.altBg + ' ' + props.dark.altText
                    : props.light.altBg + ' ' + props.light.altText)
                }
              >
                Website:
              </strong>
            ) : (
              <></>
            )}{' '}
            <a
              className="underline decoration-1"
              target="_blank"
              href={entry.url}
            >
              {entry.url}
            </a>
          </div>
          <div id="media" className="grid grid-cols-1">
            {entry.media.map((image, i) => {
              return (
                <img
                  className="col-span-1 m-auto my-2 rounded-lg"
                  src={image}
                  key={i}
                />
              );
            })}
          </div>
        </div>
      </div>
    </motion.div>
  );
};

export default Works;
