import { FC, useState, useEffect, useContext } from 'react';
import './App.css';
import { Outlet, useLocation } from 'react-router-dom';
import Nav from './pages/Nav';
import { ThemeContext } from './ThemeContext';
import { motion } from 'framer-motion';

interface AppProps {
  page: string;
}

const App: FC<AppProps> = ({ page }) => {
  const { props, setProps } = useContext(ThemeContext);
  const [title, setTitle] = useState('');

  const location = useLocation();
  let path = location.pathname.split('/').slice(1);
  const formattedPath = path.map((str) => {
    if (str) {
      return str[0].toUpperCase() + str.slice(1);
    }
  });
  const formattedTitle = formattedPath[formattedPath.length - 1] + ' - ';
  useEffect(() => {
    if (path[0].length > 1) {
      setTitle(formattedTitle + 'Wyatt Horton');
    } else {
      setTitle('Wyatt Horton - Home');
    }
  }, [location]);
  document.title = title;

  return (
    <div
      className={
        'App flex flex-col h-full min-h-screen ' +
        (props.isDark
          ? props.dark.bg + ' ' + props.dark.text + ' ' + props.dark.border
          : props.light.bg + ' ' + props.light.text + ' ' + props.light.border)
      }
    >
      <motion.div animate={{ scale: 0.5 }} />
      <div
        className={
          'App w-full md:w-3/6 xl:w-2/6 3xl:w-1/5 mx-auto ' +
          (props.isDark
            ? props.dark.bg + ' ' + props.dark.text + ' ' + props.dark.border
            : props.light.bg +
              ' ' +
              props.light.text +
              ' ' +
              props.light.border)
        }
      >
        <Nav setDarkMode={setProps} />
        <Outlet />
      </div>
    </div>
  );
};

export default App;
