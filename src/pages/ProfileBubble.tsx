import React, { FC, useState, useEffect, useContext } from 'react';
import { ThemeContext } from '../ThemeContext';

interface ProfileBubbleProps {
  src: string;
  name: string;
  blurb: string;
}

const ProfileBubble: FC<ProfileBubbleProps> = ({ src, name, blurb }) => {
  return (
    <>
      <div className="py-4 flex flex-row">
        <img
          src={src}
          alt="me"
          className="border-none w-1/4 rounded-full m-auto ml-4"
        />
        <div id="titleCard" className="m-auto pl-4">
          <h1 className="text-left text-xl">
            <strong>{name}</strong>
          </h1>
          <div className="">
            <h3 className="text-left inline-block text-lg">{blurb}</h3>
          </div>
        </div>
      </div>
    </>
  );
};

export default ProfileBubble;
