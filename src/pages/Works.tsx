import { FC, useContext } from 'react';
import { Link } from 'react-router-dom';
import { ThemeContext } from '../ThemeContext';

interface WorksProps {
  works: {
    work: string;
    description: string;
    route: string;
    src: string;
    id: number;
  }[];
}

const Works: FC<WorksProps> = ({ works }) => {
  const { props, setProps } = useContext(ThemeContext);
  return (
    <>
      <div className=" w-full pt-2">
        <div className="grid grid-cols-1  md:grid-cols-2 gap-4">
          {works.map((work) => {
            return (
              <div className="grid grid-cols-1 gap-4" key={work.id}>
                <Link className="inline" to={work.route}>
                  <div
                    className={
                      'col-span-2 ' +
                      (props.isDark ? props.dark.border : props.light.border)
                    }
                  >
                    <img
                      className="rounded-lg w-full"
                      src={work.src}
                      alt={work.route}
                    />
                    <strong className="text-center">{work.work}</strong>
                    <br></br>
                    <div className="text-justify text-start w-1/2 md:w-3/4 m-auto">
                      {work.description}
                    </div>
                  </div>
                </Link>
              </div>
            );
          })}
        </div>
      </div>
    </>
  );
};

export default Works;
