import React, { useState, useEffect, ReactNode } from 'react';
interface Props {
  children?: ReactNode;
  // any props that come into the component
}

interface ContextProps {
  props: {
    isDark: boolean;
    dark: {
      bg: string;
      altBg: string;
      text: string;
      border: string;
      linkBgColor: string;
      altText: string;
      svgFill: string;
      navBg: string;
    };
    light: {
      bg: string;
      altBg: string;
      text: string;
      border: string;
      linkBgColor: string;
      altText: string;
      svgFill: string;
      navBg: string;
    };
  };
  setProps: Function;
}

const setProps = () => {};
const props = {
  isDark: false,
  dark: {
    bg: 'bg-zinc-800',
    altBg: '',
    text: 'text-gray-300',
    border: 'border-slate-500',
    linkBgColor: 'bg-cyan-300',
    altText: 'text-gray-200',
    svgFill: '',
    navBg: ''
  },
  light: {
    bg: 'bg-orange-100',
    altBg: '',
    text: 'text-slate-600',
    border: 'border-red-400',
    linkBgColor: 'bg-teal-700',
    altText: 'text-white',
    svgFill: '',
    navBg: ''
  },
  setProps: setProps
};

export const ThemeContext = React.createContext<ContextProps>({
  props,
  setProps
});
export const ContextProvider = ({ children }: Props) => {
  const [props, setProps] = useState({
    isDark: false,
    dark: {
      bg: 'bg-zinc-800',
      altBg: 'bg-fuchsia-400',
      text: 'text-gray-300',
      border: 'border-slate-500',
      linkBgColor: 'bg-cyan-100',
      altText: 'text-black',
      svgFill: 'white',
      navBg: 'bg-neutral-600'
    },
    light: {
      bg: 'bg-orange-100',
      altBg: 'bg-teal-700',
      text: 'text-slate-600',
      border: 'border-red-400',
      linkBgColor: 'bg-teal-700',
      altText: 'text-white',
      svgFill: 'black',
      navBg: 'bg-stone-50'
    }
  });

  return (
    <ThemeContext.Provider value={{ props, setProps }}>
      {children}
    </ThemeContext.Provider>
  );
};
