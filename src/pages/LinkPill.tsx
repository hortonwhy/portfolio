import React, { FC, useState, useEffect, useContext } from 'react';
import { ThemeContext } from '../ThemeContext';

interface LinkPillProps {
  href: string;
  src: string;
  text: string;
}

const LinkPill: FC<LinkPillProps> = ({ href, src, text }) => {
  const { props, setProps } = useContext(ThemeContext);
  return (
    <a href={href} target="_blank" rel="noreferrer" className="mt-2">
      <div
        className={
          'px-4 opacity-70 border border-hidden rounded hover:opacity-100 flex flex-row ' +
          (props.isDark
            ? props.dark.linkBgColor + ' ' + props.dark.altText
            : props.light.linkBgColor + ' ' + props.light.altText)
        }
      >
        <img className="mr-2" src={src} alt="gitlab"></img>
        {text}
      </div>
    </a>
  );
};

export default LinkPill;
