import React, { FC, useState, useEffect, useContext } from 'react';
import { ThemeContext } from '../ThemeContext';
import LinkPill from './LinkPill';
import ProfileBubble from './ProfileBubble';
import Timeline from './TimelineElement';
import { motion } from 'framer-motion';

const Body: FC = () => {
  const { props, setProps } = useContext(ThemeContext);
  console.log(props.isDark);
  useEffect(() => {
    console.log('body update');
  }, [props]);

  const events = [
    {
      year: 2018,
      event: 'Entered the University of Texas at Austin'
    },
    {
      year: 2022,
      event:
        'Completed the Bachelor of Arts in Japanese Language and Culture degree track as well as the Computer Science certificate program from the University of Texas at Austin'
    }
  ];

  return (
    <>
      <motion.div
        animate={{ scale: [0.75, 1], opacity: [0, 1], x: [100, 0] }}
        transition={{ duration: 0.6 }}
        className={'flex flex-col w-4/5 m-auto border-inherit pb-4'}
      >
        <div className="my-4">
          <ProfileBubble
            src="https://minioapi.wyatthorton.xyz/portfolio/download/grad-pic.jpg"
            name="Wyatt Horton"
            blurb="
              Principled Individual: lifelong student, developer, dreamer,
              advocate"
          />
        </div>
        <div className="border-inherit">
          <div
            className={
              'border-b-2 w-1/6 ml-8 px-2 pt-6 text-left border-inherit'
            }
          >
            Bio
          </div>
          <div className="text-justify w-fit pt-2">
            Wyatt is a graduate from the University of Texas at Austin who has a
            certificate in computer science and bachelors in Japanese. He
            ascertains much enjoyment from building and deploying his own
            projects. He enjoys the diversity and freedom of direction that
            comes with fullstack development, he relishes in the opportunity to
            be creative with front-end development, and enjoys the challenge of
            designing efficient and concise backend services.
          </div>
        </div>
        <div className="border-inherit">
          <div
            className={
              'border-b-2 w-1/6 ml-8 px-2 pt-6 text-left border-inherit'
            }
          >
            Timeline
          </div>
          <Timeline events={events} />
        </div>

        <div className="border-inherit">
          <h1
            className={
              'border-b-2 w-1/6 ml-8 px-2 pt-6 text-left border-inherit'
            }
          >
            Interests
          </h1>
          <div className="text-justify w-fit pt-2">
            Wyatts interest range from reading, learning Japanese, lifting
            weights, running, rock-climbing, to coding in his free time. Some
            impactful books that he has read have been Cal Newport's Digital
            Minimalism and James Clear's Atomic habits. His favorite keyboard is
            the Kinesis Advantage 2. And his choice of editor is Emacs in evil
            mode. Server Distro: Gentoo, Laptop: Arch, Desktop: Arch (for now).
          </div>
        </div>
        <div className="border-inherit">
          <h1
            className={
              'border-b-2 md:w-1/6 ml-8 px-2 pt-6 text-left border-inherit'
            }
          >
            Links
          </h1>
          <div className="text-justify w-fit flex flex-col">
            <LinkPill
              href="https://gitlab.com/hortonwhy"
              src="https://gitlab.com/gitlab-org/gitlab-svgs/-/raw/main/sprite_icons/tanuki.svg"
              text="@hortonwhy"
            />
            <LinkPill
              href="https://github.com/hortonwhy"
              src="https://gitlab.com/gitlab-org/gitlab-svgs/-/raw/main/sprite_icons/github.svg"
              text="@hortonwhy"
            />
            <LinkPill
              href="https://www.linkedin.com/in/wyatt-horton-809225240/"
              src="https://gitlab.com/gitlab-org/gitlab-svgs/-/raw/main/sprite_icons/linkedin.svg"
              text="Wyatt Horton"
            />
            <LinkPill
              href="https://minioapi.wyatthorton.xyz/portfolio/download/Horton_Wyatt_Resume.pdf"
              src="https://gitlab.com/gitlab-org/gitlab-svgs/-/raw/main/sprite_icons/download.svg"
              text="Resume"
            />
            <LinkPill
              href="mailto:hortonwhy@protonmail.com"
              src="https://gitlab.com/gitlab-org/gitlab-svgs/-/raw/main/sprite_icons/mail.svg?inline=false"
              text="hortonwhy@protonmail.com"
            />
          </div>
        </div>
      </motion.div>
    </>
  );
};

export default Body;
