import React, { FC, useContext } from 'react';
import { ThemeContext } from '../ThemeContext';
import Works from '../pages/Works';
import { motion } from 'framer-motion';

interface AboutProps {
  title: string;
}

const About: FC<AboutProps> = ({ title }) => {
  console.log('about page loaded...');
  const { props, setProps } = useContext(ThemeContext);

  const works = [
    {
      work: 'Portfolio Website',
      description: 'React and TailwindCSS website written in Typescript',
      route: 'portfolio',
      src: 'https://minioapi.wyatthorton.xyz/portfolio/download/portfolio-home.png',
      id: 1
    },
    {
      work: 'Pet-updates',
      description:
        'A website I made for when I was house-sitting for my sister; React, TailwindCSS, TypeORM, Typescript, Postgres',
      route: 'pet-updates',
      src: 'https://minioapi.wyatthorton.xyz/portfolio/download/pet-updates.png',
      id: 2
    },
    {
      work: 'Self-Hosted Server',
      description:
        'A server running Gentoo Linux I built, setup, and use to host various projects, services, and tools for my own personal use. Leveraged Docker to organize and host all services',
      route: 'linux-server',
      src: 'https://minioapi.wyatthorton.xyz/portfolio/download/gentoo-server.png',
      id: 3
    },
    {
      work: 'Phaser Game',
      description:
        'Neverending Platformer coded in the JavaScript framework Phaser',
      route: 'phaser-game',
      src: 'https://minioapi.wyatthorton.xyz/portfolio/download/slime/slime-cover.png',
      id: 4
    },
    {
      work: 'DokiDoki Travel',
      description:
        'A website for individuals who are looking to travel to the locations where their favorite anime took place',
      route: 'dokidoki-travel',
      src: 'https://minioapi.wyatthorton.xyz/portfolio/download/dokidoki/titlecard.png',
      id: 5
    },
    {
      work: 'Ecommerce [Dropped Project]',
      description:
        'an Ecommerce website I wanted to make for a friend before realizing the scope was too large for only myself',
      route: 'ecommerce',
      src: 'https://minioapi.wyatthorton.xyz/portfolio/download/ecommerce/home.png',
      id: 5
    }
  ];

  return (
    <motion.div
      animate={{ scale: [0.75, 1], opacity: [0, 1], x: [-100, 0] }}
      transition={{ duration: 0.6 }}
      className=""
    >
      <div
        className={
          'border-b-2 w-1/6 ml-8 px-2 pt-6 mb-4 text-left ' +
          (props.isDark ? props.dark.border : props.light.border)
        }
      >
        Works
      </div>
      <Works works={works} />
    </motion.div>
  );
};

export default About;
