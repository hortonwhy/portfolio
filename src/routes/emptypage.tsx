import React, { FC } from 'react';

interface EmptyPageProps {
  content: string;
}

const EmptyPage: FC<EmptyPageProps> = (props: EmptyPageProps) => {
  return (
    <>
      <h1>{props.content}</h1>
    </>
  );
};

export default EmptyPage;
